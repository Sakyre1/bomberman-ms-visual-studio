#pragma once

#include <iostream>
#include <SFML\Graphics.hpp>
#include "Coin.h"
#include "Block.h"
#include "Grid.h"
#include <string>
#include <math.h>

class Player {
public:
	Player(std::string playerName, sf::String fT, sf::String bT , sf::String rlT, sf::Vector2f position, int controlls, int newRadius) {
		controllType = controlls;
		p_name = playerName;
		if (!xAxis.loadFromFile(rlT)) {
			//std::cout << "Error loading resource '::Texture'" << std::endl;
		}
		if (!yAxisFront.loadFromFile(fT)) {
			//std::cout << "Error loading resource '::Texture'" << std::endl;
		}
		if (!yAxisBack.loadFromFile(bT)) {
			//std::cout << "Error loading resource '::Texture'" << std::endl;
		}
		bombRadius = newRadius;
		player.setTexture(xAxis);
		player.setPosition(position);
		player.setOrigin(sf::Vector2f(0, 0));
	}

	void drawTo(sf::RenderWindow &window) {
		window.draw(player);
	}

	//Check if player is in an explosion
	bool isInExplosion(Grid gridBlock) {
		if (xy == gridBlock.getXY()) {
			//std::cout << xy.x << " " << xy.y << " |" << gridBlock.getXY().x << " " << gridBlock.getXY().y << std::endl;
			return true;
		}
		return false;
		//If player  is not immunte to damage go further
		/*if (!immuneToDamage && !immunityIsTicking) {
			//std::cout << "Not immune to damage: "<< p_name << std::endl;
			//If character is in the same spot as the explosion go further
			if (xy == gridBlock.getXY()) {
				//std::cout << p_name << " is standing on the explosion [" << xy.x << ";" << xy.y << "], [" << gridBlock.getXY().x << ";" << gridBlock.getXY().y << "]" << std::endl;
				//Set players immunity to damage to true
				//std::cout << xy.x << " " << xy.y << " |" << gridBlock.getXY().x << " " << gridBlock.getXY().y << std::endl;
				//std::cout << "In range!" << std::endl;
				immuneToDamage = true;
			}else {
				//std::cout << p_name << " is standing somewhere else" << std::endl;
			}
		}else {
			//std::cout << "Immune to damage: "<< p_name << std::endl;
		}*/
	}

	//Set players current grid position
	void setLocationXY(sf::Vector2f newPosition) {
		xy = newPosition;
	}

	//Get player current location
	sf::Vector2f currentLocation() {
		return xy;
	}
	//Move player
	void move(sf::Vector2f distance) {
		player.move(distance);
	}
	//Set player's position
	void setPos(sf::Vector2f newPos) {
		player.setPosition(newPos);
	}
	//Set player's score
	void setScore(int newScore) {
		score += newScore;
	}
	//Get player's score
	int getScore() {
		return score;
	}

	//Check collision with coins
	bool isCollidingWithCoin(Coin *coin) {
		if (player.getGlobalBounds().intersects(coin->getGlobalBounds())) {
			return true;
		}
		return false;
	}
	//Check collision with blocks
	bool isCollidingWithBlock(Block *block) {
		if (player.getGlobalBounds().intersects(block->getGlobalBounds())) {
			return true;
		}
		return false;
	}
	//Check collision with other players
	bool isCollidingWithPlayer(Player *otherPlayer) {
		if (player.getGlobalBounds().intersects(otherPlayer->player.getGlobalBounds())) {
			return true;
		}
		return false;
	}
	//Check if players is on one of the grid blocks
	bool isCollidingWithGridBlock(Grid *gridBlock) {
		if (player.getGlobalBounds().intersects(gridBlock->getGlobalBounds())) {
			return true;
		}
		return false;
	}

	void Update(sf::RenderWindow &window, std::vector<Block*> &blockVec, Player *p2, std::vector<Grid*> &gridVec) {
		if (keyTime < 8) {
			keyTime++;
		}
		switch (controllType){
			case 0:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && player.getPosition().x > 96) {
					player.move(-5.f, 0.f);
					//Collsion between other players
					if (isCollidingWithPlayer(p2)) {
						player.move(5.f, 0.f);
					}
					//Collsion between blocks
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(5.f, 0.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(5.f, 0.f);
						}
					}
					//Check on which grid's block player is standing
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					//#Checking grid
					player.setTexture(xAxis);
					player.setTextureRect(sf::IntRect(player.getGlobalBounds().width, 0, -player.getGlobalBounds().width, player.getGlobalBounds().height));
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && player.getPosition().x + player.getGlobalBounds().width < window.getSize().x - 96) {
					player.move(5.f, 0.f);
					if (isCollidingWithPlayer(p2)) {
						player.move(-5.f, 0.f);
					}
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(-5.f, 0.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(-5.f, 0.f);
						}
					}
					//Check on which grid's block player is standing
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					//#Checking grid
					player.setTexture(xAxis);
					player.setTextureRect(sf::IntRect(0, 0, player.getGlobalBounds().width, player.getGlobalBounds().height));
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && player.getPosition().y > 32) {
					player.move(0.f, -5.f);
					if (isCollidingWithPlayer(p2)) {
						player.move(0.f, 5.f);
					}
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(0.f, 5.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(0.f, 5.f);
						}
					}
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					player.setTexture(yAxisBack);
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && player.getPosition().y + player.getGlobalBounds().height < window.getSize().y-32) {
					player.move(0.f, 5.f);
					if (isCollidingWithPlayer(p2)) {
						player.move(0.f, -5.f);
					}
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(0.f, -5.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(0.f, -5.f);
						}
					}
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					player.setTexture(yAxisFront);
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
					for (int i = 0; i < gridVec.size(); i++) {
						if (sf::Vector2f{ currentLocation() } != sf::Vector2f{-1.f, -1.f}){
							if (sf::Vector2f{ gridVec[i]->getXY()} == sf::Vector2f{currentLocation()} && !gridVec[i]->isBombPlaced()) {
								//If bomb is not placed and there is no explosion then PLACE A BOMB
								if (!gridVec[i]->isBombPlaced() && !gridVec[i]->hasBombExploded() && !gridVec[i]->checkIfTable()) {
									if (bombs <= 0) {
										//std::cout << "Reached maximum bomb drop ammount!" << std::endl;
										break;
									}
									//std::cout << "Reached maximum bomb drop ammount! "<< bombs << std::endl;
									bombs -= 1;
									gridVec[i]->setBombUp(true);
									gridVec[i]->setBombRadius(bombRadius);
									gridVec[i]->setBombTime();
									//gridVec[i]->setColor(sf::Color::Green);
									//gridVec[i]->drawTo(window);
									gridVec[i]->setTexture("img/bomb_paths/bomb.png");
									//std::cout << currentLocation().x << " " << currentLocation().y << std::endl;
								}
							}
						}
					}
				}
				break;
			case 1:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1) && player.getPosition().x > 96) {
					player.move(-5.f, 0.f);
					//Collsion between other players
					if (isCollidingWithPlayer(p2)) {
						player.move(5.f, 0.f);
					}
					//Collsion between blocks
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(5.f, 0.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(5.f, 0.f);
						}
					}
					//Check on which grid's block player is standing
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					//#Checking grid
					player.setTexture(xAxis);
					player.setTextureRect(sf::IntRect(player.getGlobalBounds().width, 0, -player.getGlobalBounds().width, player.getGlobalBounds().height));
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3) && player.getPosition().x + player.getGlobalBounds().width < window.getSize().x - 96) {
					player.move(5.f, 0.f);
					if (isCollidingWithPlayer(p2)) {
						player.move(-5.f, 0.f);
					}
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(-5.f, 0.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(-5.f, 0.f);
						}
					}
					//Check on which grid's block player is standing
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					//#Checking grid
					player.setTexture(xAxis);
					player.setTextureRect(sf::IntRect(0, 0, player.getGlobalBounds().width, player.getGlobalBounds().height));
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad5) && player.getPosition().y > 32) {
					player.move(0.f, -5.f);
					if (isCollidingWithPlayer(p2)) {
						player.move(0.f, 5.f);
					}
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(0.f, 5.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(0.f, 5.f);
						}
					}
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					player.setTexture(yAxisBack);
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2) && player.getPosition().y + player.getGlobalBounds().height < window.getSize().y - 32) {
					player.move(0.f, 5.f);
					if (isCollidingWithPlayer(p2)) {
						player.move(0.f, -5.f);
					}
					for (int i = 0; i < blockVec.size(); i++) {
						if (isCollidingWithBlock(blockVec[i]) && blockVec[i]->isItActive()) {
							player.move(0.f, -5.f);
						}
					}
					//Check if player is standing on a bomb
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable() && gridVec[i]->isBombCollidable()) {
							player.move(0.f, -5.f);
						}
					}
					for (int i = 0; i < gridVec.size(); i++) {
						if (isCollidingWithGridBlock(gridVec[i]) && !gridVec[i]->checkIfTable()) {
							setLocationXY(gridVec[i]->getXY());
						}
					}
					player.setTexture(yAxisFront);
					keyTime = 0;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad0)) {
					for (int i = 0; i < gridVec.size(); i++) {
						for (int i = 0; i < gridVec.size(); i++) {
							if (sf::Vector2f{ currentLocation() } != sf::Vector2f{ -1.f, -1.f }) {
								if (sf::Vector2f{ gridVec[i]->getXY() } == sf::Vector2f{ currentLocation() } && !gridVec[i]->isBombPlaced()) {
									if (!gridVec[i]->isBombPlaced() && !gridVec[i]->hasBombExploded() && !gridVec[i]->checkIfTable()) {
										if (bombs <= 0) {
											//std::cout << "Reached maximum bomb drop ammount!" << std::endl;
											break;
										}
										//std::cout << "Reached maximum bomb drop ammount! "<< bombs << std::endl;
										bombs -= 1;
										gridVec[i]->setBombUp(true);
										gridVec[i]->setBombRadius(bombRadius);
										gridVec[i]->setBombTime();
										//gridVec[i]->setColor(sf::Color::Green);
										//gridVec[i]->drawTo(window);
										gridVec[i]->setTexture("img/bomb_paths/bomb.png");
										//std::cout << currentLocation().x << " " << currentLocation().y << std::endl;
									}
								}
							}
						}
					}
					/*bombs++;
					std::cout << "DROP BOMB:" << bombs << std::endl;*/
				}
				break;
			default:
				std::cout << "Fuck off. You're not invited" << std::endl;
				break;
		}
	}

	//Reset immunity
	void resetImmunity() {
		if (immunityTimer <= 0 && immuneToDamage) {
			immuneToDamage = false;
			lives--;
			std::cout << "Setting OFF immunity" << std::endl;
		}
	}
	//Reset bomb count timer
	void resetBombCounterTimer() {
		if (bombDropTime <= 0) {
			if ((maxBombs - bombs) > 0) {
				bombs++;
			}
			bombDropTime = defaultBombDropTime;
			//std::cout << "Adding another bomb: " << bombs << std::endl;
		}
	}
	//Update bomb drop timer
	void updateBombDropTimer(float currentTime) {
		if (bombDropTime > 0) {
			bombDropTime -= currentTime;
		}
		//std::cout << immunityTimer << std::endl;
	}

	//Set time when player got hit
	void setHitTime(float currentTime) {
		if (immunityTimer <= 0 && !immuneToDamage) {
			immuneToDamage = true;
			immunityTimer = coolDown;
			std::cout << "Setting ON immunity "<< p_name << " " << immuneToDamage << " " << immunityTimer << std::endl;
		}
	}
	//Check if player is already immune to damage
	bool isImmune() {
		if (immunityTimer > 0) {
			//std::cout << "Immune " << p_name << std::endl;
			return true;
		}
		//std::cout << "Not immune" << std::endl;
		return false;
	}
	//Update immunity timer
	void updateImmunityTimer(float currentTime) {
		if (immunityTimer > 0) {
			immunityTimer -= currentTime;
		}
		//std::cout << immunityTimer << std::endl;
	}

	void setImmunity(bool qt) {
		immuneToDamage = qt;
	}

	bool isPlayerImmune() {
		return immuneToDamage;
	}

	int getLives() {
		return lives;
	}

	//Remove player's immunity to damage after cooldown and time when player got hit is greater or equalt to games passed time
	bool removeImmunity(float currentTime) {
		//std::cout << timeWhenHit << " " << currentTime << std::endl;
		if (currentTime >= (timeWhenHit + coolDown) && immuneToDamage) {
			//std::cout << currentTime << ">=" << timeWhenHit << " + " << coolDown << std::endl;
			//immuneToDamage = false; 
			//timeWhenHit = 0;
			//lives--;
			//std::cout << "Immunity removed: "<< lives << std::endl;
			//std::cout << "removed immunity to damage" << std::endl;
			return true;
		}
		return false;
	}
	//Get player's Y position
	int getY() {
		return player.getPosition().y;
	}

	//Set drop bomb key
	void setKeyState(bool ans) {
		bombDropped = ans;
	}
	void setBombCount(int ammount) {
		maxBombs += ammount;
	}

private:
	sf::Texture yAxisFront;
	sf::Texture yAxisBack;
	sf::Texture xAxis;
	//Player location
	sf::Vector2f xy;
	//int currentX;
	//int currentY;
	//sf::RectangleShape player;
	sf::Sprite player;
	//Player name
	std::string p_name;
	float speed = 5.f;
	int keyTime = 0;
	//Player lives
	int lives = 3;
	bool immuneToDamage = false;
	float coolDown = 5.0; //Immunity time value
	float bombCoolDown = 5.0;
	float immunityTimer = 0.0; //Set immunity timer here
	float timeWhenHit = 0.0; // if(timeWhenHit + coolDown >= elapsedTime) then immuneToDamage = false;
	int bombs = 1;
	int maxBombs = 1;
	float bombDropTime = 5.0; //After every 'defaultBombDropTime' add 'bombs' to player (Cannot go above 'maxBombs')
	float defaultBombDropTime = 5.0; //After every 'defaultBombDropTime' add 'bombs' to player (Cannot go above 'maxBombs')
	int lastTimeBombDropped;
	int bombRadius = 1; //1+n - Destroys 1+n block around the bomb
	//Player score
	int score = 0;
	//Which keys player is using (1 - 2)
	bool bombDropped = false; //If Space or 0 key was pressed then set it to true and drop a bomb where the player is standing
	int controllType; //1 - WASD+SPACE, 2 - 5123+0;
};






























//Y U NO WORK LOGIC!

//Check if the player is in the explosion zone
//If true set immune to TRUE and set immunity timer to be equal to cooldown
//Make immunity to be subtracted from elapsed time
//Then check if it's <= 0. If it is TRUE then removed immunity
//Set the immunity back to zero if it's lower than zero
//Repeat



//Delete/Replace:
//grid[i][j + 1 * b].setBombUp(false)
//for they are only for where the bomb is placed and not where explosions will occur
//Which is another story to tell

//If
//1) Player is not immune to explosion
//2) Player is in the explosion range
//3) Player is in the explosion when it is set to true?