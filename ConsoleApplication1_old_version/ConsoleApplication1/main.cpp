#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <string>
#include <sstream>
#include "Player.h"
#include "Coin.h"
#include "Block.h"
#include "Grid.h"

const int xGrid = 13;
const int yGrid = 11;

int main() {
	//Clock Object:
	sf::Clock clock;
	//#Clock

	bool gameOn = true;

	//Time
	sf::Time elapsed;
	float t;
	//#Time


	//Sounds
	sf::SoundBuffer buffer;
	if (!buffer.loadFromFile("sounds/Explosion.wav")) {
		return -1;
	}
	sf::Sound sound;
	sound.setBuffer(buffer);
	//#Sounds

	//Music
	sf::Music music;

	// Open it from an audio file
	if (!music.openFromFile("music/TAXI_4.wav")){
		std::cout << "no music found";
	}
	music.setVolume(50);         // reduce the volume
	music.setLoop(true);
	music.play();

	bool gameOverSound = false;

	//#Music

	//Player Object:
	Player p1("Player1", "img/p1_front.png", "img/p1_back.png", "img/p1_left_right.png", { 96, 32 }, 0, 1);
	Player p2("Player2", "img/p2_front.png", "img/p2_back.png", "img/p2_left_right.png", { 864, 32 }, 1, 1);

	//Window Object:
	sf::RenderWindow window(sf::VideoMode(1024, 768), "Terrorist Kid - Alpha Mode");
	window.setFramerateLimit(60);

	//Load map texture
	sf::Texture mapTexture;
	if (!mapTexture.loadFromFile("img/map.png")) {
		std::cout << "failed to load map.pgn" << std::endl;
	}
	//Set map texture
	sf::Sprite map;
	map.setTexture(mapTexture);

	//Grid Object:
	std::vector<Grid*> gridVec;
	Grid grid[xGrid][yGrid];
	//Set paramaters for grid 
	for (int i = 0; i < xGrid; i++) {
		for (int j = 0; j < yGrid; j++) {
			float xCoord, yCoord;
			std::string name;
			xCoord = 96 + i * 64;
			yCoord = 32 + j * 64;
			name = "[" + std::to_string(i) + " " + std::to_string(j) + "]";
			grid[i][j].setName(name);
			grid[i][j].setXY(i, j);
			//grid[i][j].setStyle({ 64, 64 }, { xCoord, yCoord }, sf::Color::Green);
			grid[i][j].setStyle({ 64, 64 }, { xCoord, yCoord }, sf::Color::Transparent);
			gridVec.push_back(&grid[i][j]);
		}
	}
	//#Set paramaters for grid 

	//Set tables
	for (int i = 1; i < xGrid; i = i + 2) {
		for (int j = 1; j < yGrid; j = j + 2) {
			float xCoord, yCoord;
			xCoord = 96 + i * 64;
			yCoord = 32 + j * 64;
			//Cannot be interacted with
			grid[i][j].setItAsTable(true);
			//grid[i][j].setStyle({ 64, 64 }, { xCoord, yCoord }, sf::Color::Magenta);
			grid[i][j].setStyle({ 64, 64 }, { xCoord, yCoord }, sf::Color::Transparent);
		}
	}
	//#Set tables

	//#Grid

	//Block Object:
	std::vector<Block*> blockVec;
	Block block[xGrid][yGrid];
	//Set everything to destructable blocks, that can not be pass through
	for (int i = 0; i < xGrid; i++) {
		for (int j = 0; j < yGrid; j++) {
			float xCoord, yCoord;
			xCoord = 96 + i * 64;
			yCoord = 32 + j * 64;
			block[i][j].setStyle({ 64, 64 }, { xCoord, yCoord }, sf::Color::Transparent, true, true, "img/pc.png");
			//block[i][j].setStyle({ 64, 64 }, { xCoord, yCoord }, sf::Color::Transparent, false, false, "img/empty.png");
			blockVec.push_back(&block[i][j]);
		}
	}
	//#Set everything to destructable blocks, that can not be pass through

	//Set every 2nd block to indestructable block that can not be destroyed
	for (int i = 1; i < xGrid; i = i + 2) {
		for (int j = 1; j < yGrid; j = j + 2) {
			float xCoord, yCoord;
			xCoord = 96 + i * 64;
			yCoord = 32 + j * 64;
			block[i][j].setStyle({ 64, 64 }, { xCoord, yCoord }, sf::Color::Transparent, true, false, "img/empty.png");
		}
	}
	//#Set every 2nd block to indestructable block that can not be destroyed

	//#Block

	block[0][0].setCollision(false);
	block[0][1].setCollision(false);
	block[1][0].setCollision(false);
	block[0][0].setDestructable(false);
	block[0][1].setDestructable(false);
	block[1][0].setDestructable(false);
	block[12][0].setCollision(false);
	block[12][1].setCollision(false);
	block[11][0].setCollision(false);
	block[0][0].setDestructable(false);
	block[0][1].setDestructable(false);
	block[1][0].setDestructable(false);

	//Coins
	std::vector<Coin*> coinVec;

	//1 - Increases range
	//2 - Increases bomb count
	//3 - Reduce bomb count down?
	//4 - Reduce explosion count down?
	//5 - Decreate the time for the next bomb?
	//6 - Increase player speed?
	//Coin coins[25];

	//#Coins


	//Score Objects:
	sf::Font arial;
	arial.loadFromFile("fonts/arial.ttf");

	std::ostringstream ssScore;
	ssScore << "Score: P1 - " << (int)p1.getScore() << ", P2 - " << (int)p2.getScore() << ", Lives: P1 - " << p1.getLives() << ", P2 - " << p2.getLives();

	sf::Text lblScore;
	lblScore.setCharacterSize(24);
	lblScore.setPosition({ 0, 0 });
	lblScore.setFont(arial);
	lblScore.setString(ssScore.str());
	//#Score Object

	bool minutePassed = false;

	p1.setLocationXY({ 0, 0 });
	p2.setLocationXY({ 12, 0 });

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch (event.type) {
				case sf::Event::Closed: {
					window.close();
				}
				break;
				case sf::Event::KeyReleased: {
					if (event.key.code == 57) {
						//p1.setKeyState(true);
						//std::cout << "Space bar" << std::endl;
					}else if (event.key.code == 75) {
						//p2.setKeyState(true);
						//std::cout << "0 key" << std::endl;
					}else if (event.key.code == 67) {
						//std::cout << event.key.code << std::endl;
						p1.setBombCount(1);
					}else if (event.key.code == 68) {
						//std::cout << event.key.code << std::endl;
						p1.setBombCount(-1);
					}else if (event.key.code == 69) {
						gameOn = true;
						if (!mapTexture.loadFromFile("img/map.png")) {
							std::cout << "failed to load map.pgn" << std::endl;
						}
						map.setTexture(mapTexture);
						window.draw(map);
						//std::cout << event.key.code << std::endl;
					}else if (event.key.code == 70) {
						gameOn = false;
						//std::cout << event.key.code << std::endl;
					}
				}
				break;
			}
		}
		if(gameOn){
			
			elapsed = clock.getElapsedTime();
			t = elapsed.asSeconds();

			if (p1.getLives() <= 0 || p2.getLives() <= 0) {
				gameOn = false;
			}

			//Check if any of the grid blocks where set as a bomb by the Player1 or Player2
			for (int i = 0; i < xGrid; i++) {
				for (int j = 0; j < yGrid; j++) {
					if (grid[i][j].isBombPlaced()) {
						//grid[i][j].setColor(sf::Color::Red);
					}
					//If bomb is ticking and bomb is placed
					if (grid[i][j].getBombCountDownTime() && grid[i][j].isBombPlaced()) {
						//Update the bomb timer
						grid[i][j].updateBombTimer(0.1234f);
						grid[i][j].updateCollisionTimer(0.01234f);
						//If the timer reached 0 removed the bomb and set bombExploded to true
						grid[i][j].removeBomb(); //Set to Color - Black, bombExploded = true
						if (grid[i][j].hasBombExploded()) {
							//Set the time for the explosion to fade
							grid[i][j].setTexture("img/bomb_paths/boom.png");
							grid[i][j].setBombExploded(true);
							sound.play();
							grid[i][j].setExplosionTime();
							//What do we need for this:
							//1. X, Y grid coordinates where explosion began
							
							//Start an explosion chain event | Domino effect
							for (int m = 0; m < xGrid; m++) {
								for (int n = 0; n < yGrid; n++) {
									int radius = grid[i][j].getBombRadius();
									//std::cout << radius << std::endl;
									if (m == 0 && n == 0 && i == m && n == j) {
										//Bomb explosion radius
										for (int b = 1; b <= radius; b++) {
											//While explosion is shorter than yGrid
											if ((n + 1 * b < yGrid)) {
												if (!grid[m][n + 1 * b].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m][n + 1 * b].isItDestructable()) {
														block[m][n + 1 * b].setCollision(false);
														block[m][n + 1 * b].setDestructable(false);
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														break;
													}
													if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
														break;
													}
													grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
													grid[m][n + 1 * b].setBombExploded(true);
													grid[m][n + 1 * b].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
										for (int b = 1; b <= radius; b++) {
											if ((m + 1 * b < xGrid)) {
												if (!grid[m + 1 * b][n].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m + 1 * b][n].isItDestructable()) {
														block[m + 1 * b][n].setCollision(false);
														block[m + 1 * b][n].setDestructable(false);
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														break;
													}
													if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
														break;
													}
													grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
													grid[m + 1 * b][n].setBombExploded(true);
													grid[m + 1 * b][n].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
									}else if (m > 0 && n == 0 && m == i && n == j) {
										//std::cout << "Pirma eilute" << std::endl;
										if (m % 2 != 0) {
											//std::cout << "lyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((m - 1 * b) >= 0) {
													if (!grid[m - 1 * b][n].hasBombExploded()) {
														if (block[m - 1 * b][n].isItDestructable()) {
															block[m - 1 * b][n].setCollision(false);
															block[m - 1 * b][n].setDestructable(false);
															grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m - 1 * b][n].setBombExploded(true);
															grid[m - 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((m + 1 * b) < xGrid) {
													if (!grid[m + 1 * b][n].hasBombExploded()) {
														if (block[m + 1 * b][n].isItDestructable()) {
															block[m + 1 * b][n].setCollision(false);
															block[m + 1 * b][n].setDestructable(false);
															grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m + 1 * b][n].setBombExploded(true);
															grid[m + 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}else {
											//std::cout << "nelyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((m - 1 * b) >= 0) {
													if (!grid[m - 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m - 1 * b][n].isItDestructable()) {
															block[m - 1 * b][n].setCollision(false);
															block[m - 1 * b][n].setDestructable(false);
															grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m - 1 * b][n].setBombExploded(true);
															grid[m - 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((n + 1 * b) < yGrid) {
													if (!grid[m][n + 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n + 1 * b].isItDestructable()) {
															block[m][n + 1 * b].setCollision(false);
															block[m][n + 1 * b].setDestructable(false);
															grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n + 1 * b].setBombExploded(true);
															grid[m][n + 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((m + 1 * b) < xGrid) {
													if (!grid[m + 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m + 1 * b][n].isItDestructable()) {
															block[m + 1 * b][n].setCollision(false);
															block[m + 1 * b][n].setDestructable(false);
															grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m + 1 * b][n].setBombExploded(true);
															grid[m + 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}
									}else if (i == xGrid - 1 && j == 0 && m == i && n == j) {
										//std::cout << "BOM";
										for (int b = 1; b <= radius; b++) {
											if ((m - 1 * b >= 0)) {
												if (!grid[m - 1 * b][n].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m - 1 * b][n].isItDestructable()) {
														block[m - 1 * b][n].setCollision(false);
														block[m - 1 * b][n].setDestructable(false);
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														break;
													}
													if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
														break;
													}
													grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
													grid[m - 1 * b][n].setBombExploded(true);
													grid[m - 1 * b][n].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
										for (int b = 1; b <= radius; b++) {
											if ((n + 1 * b < yGrid)) {
												if (!grid[m][n + 1 * b].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m][n + 1 * b].isItDestructable()) {
														block[m][n + 1 * b].setCollision(false);
														block[m][n + 1 * b].setDestructable(false);
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														break;
													}
													if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
														break;
													}
													grid[m][n + 1 * b].setColor(sf::Color::Black);
													grid[m][n + 1 * b].setBombExploded(true);
													grid[m][n + 1 * b].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
									}else if (i == 0 && j == yGrid - 1 && m == i && n == j) {
										for (int b = 1; b <= radius; b++) {
											if ((n - 1 * b) >= 0) {
												if (!grid[m][n - 1 * b].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m][n - 1 * b].isItDestructable()) {
														block[m][n - 1 * b].setCollision(false);
														block[m][n - 1 * b].setDestructable(false);
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														break;
													}
													if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
														break;
													}
													grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
													grid[m][n - 1 * b].setBombExploded(true);
													grid[m][n - 1 * b].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
										for (int b = 1; b <= radius; b++) {
											if ((m + 1 * b) < xGrid) {
												if (!grid[m + 1 * b][n].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m + 1 * b][n].isItDestructable()) {
														block[m + 1 * b][n].setCollision(false);
														block[m + 1 * b][n].setDestructable(false);
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														break;
													}
													if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
														break;
													}
													grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
													grid[m + 1 * b][n].setBombExploded(true);
													grid[m + 1 * b][n].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
									}else if (i == xGrid - 1 && j == yGrid - 1 && m == i && n == j) {
										for (int b = 1; b <= radius; b++) {
											if ((m - 1 * b) >= 0) {
												if (!grid[m - 1 * b][n].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m - 1 * b][n].isItDestructable()) {
														block[m - 1 * b][n].setCollision(false);
														block[m - 1 * b][n].setDestructable(false);
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														break;
													}
													if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
														break;
													}
													grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
													grid[m - 1 * b][n].setBombExploded(true);
													grid[m - 1 * b][n].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
										for (int b = 1; b <= radius; b++) {
											if ((n - 1 * b) >= 0) {
												if (!grid[m][n - 1 * b].hasBombExploded()) {
													//DESTROY BLOCK
													if (block[m][n - 1 * b].isItDestructable()) {
														block[m][n - 1 * b].setCollision(false);
														block[m][n - 1 * b].setDestructable(false);
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														break;
													}
													if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
														break;
													}
													grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
													grid[m][n - 1 * b].setBombExploded(true);
													grid[m][n - 1 * b].setExplosionTime();
													//std::cout << "TEST" << std::endl;
												}
											}
										}
									}else if (j > 0 && i == 0 && m == i && n == j) {
										//std::cout << "Kaire eilute" << std::endl;
										if (n % 2 != 0) {
											//std::cout << "lyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((n + 1 * b) < yGrid) {
													if (!grid[m][n + 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n + 1 * b].isItDestructable()) {
															block[m][n + 1 * b].setCollision(false);
															block[m][n + 1 * b].setDestructable(false);
															grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n + 1 * b].setBombExploded(true);
															grid[m][n + 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((n - 1 * b) >= 0) {
													if (!grid[m][n - 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n - 1 * b].isItDestructable()) {
															block[m][n - 1 * b].setCollision(false);
															block[m][n - 1 * b].setDestructable(false);
															grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n - 1 * b].setBombExploded(true);
															grid[m][n - 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}else {
											//std::cout << "nelyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((n + 1 * b) < yGrid) {
													if (!grid[m][n + 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n + 1 * b].isItDestructable()) {
															block[m][n + 1 * b].setCollision(false);
															block[m][n + 1 * b].setDestructable(false);
															grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n + 1 * b].setBombExploded(true);
															grid[m][n + 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((n - 1 * b) >= 0) {
													if (!grid[m][n - 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n - 1 * b].isItDestructable()) {
															block[m][n - 1 * b].setCollision(false);
															block[m][n - 1 * b].setDestructable(false);
															grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n - 1 * b].setBombExploded(true);
															grid[m][n - 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((m + 1 * b) < xGrid) {
													if (!grid[m + 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m + 1 * b][n].isItDestructable()) {
															block[m + 1 * b][n].setCollision(false);
															block[m + 1 * b][n].setDestructable(false);
															grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m + 1 * b][n].setBombExploded(true);
															grid[m + 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}
									}else if (j > 0 && i == 12 && m == i && n == j) {
										//std::cout << "Desine eilute" << std::endl;
										if (n % 2 != 0) {
											//std::cout << "lyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((n + 1 * b) < yGrid) {
													if ((n - 1 * b) >= 0) {
														if (!grid[m][n + 1 * b].hasBombExploded()) {
															//DESTROY BLOCK
															if (block[m][n + 1 * b].isItDestructable()) {
																block[m][n + 1 * b].setCollision(false);
																block[m][n + 1 * b].setDestructable(false);
																grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
																grid[m][n + 1 * b].setBombExploded(true);
																grid[m][n + 1 * b].setExplosionTime();
																break;
															}
															if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
																break;
															}
															grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n + 1 * b].setBombExploded(true);
															grid[m][n + 1 * b].setExplosionTime();
															//std::cout << "TEST" << std::endl;
														}
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if (n - 1 * b >= 0) {
													if ((n - 1 * b) >= 0) {
														if (!grid[m][n - 1 * b].hasBombExploded()) {
															//DESTROY BLOCK
															if (block[m][n - 1 * b].isItDestructable()) {
																block[m][n - 1 * b].setCollision(false);
																block[m][n - 1 * b].setDestructable(false);
																grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
																grid[m][n - 1 * b].setBombExploded(true);
																grid[m][n - 1 * b].setExplosionTime();
																break;
															}
															if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
																break;
															}
															grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n - 1 * b].setBombExploded(true);
															grid[m][n - 1 * b].setExplosionTime();
															//std::cout << "TEST" << std::endl;
														}
													}
												}
											}
										}else {
											//std::cout << "nelyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((m - 1 * b) >= 0) {
													if (!grid[m - 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m - 1 * b][n].isItDestructable()) {
															block[m - 1 * b][n].setCollision(false);
															block[m - 1 * b][n].setDestructable(false);
															grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m - 1 * b][n].setBombExploded(true);
															grid[m - 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((n - 1 * b) >= 0) {
													if (!grid[m][n - 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n - 1 * b].isItDestructable()) {
															block[m][n - 1 * b].setCollision(false);
															block[m][n - 1 * b].setDestructable(false);
															grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n - 1 * b].setBombExploded(true);
															grid[m][n - 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if (n + 1 * b < yGrid) {
													if (!grid[m][n + 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n + 1 * b].isItDestructable()) {
															block[m][n + 1 * b].setCollision(false);
															block[m][n + 1 * b].setDestructable(false);
															grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n + 1 * b].setBombExploded(true);
															grid[m][n + 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}
									}else if (i > 0 && j == 10 && m == i && n == j) {
										//std::cout << "Paskutine eilute" << std::endl;
										if (m % 2 != 0) {
											//std::cout << "lyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((m - 1 * b) >= 0) {
													if (!grid[m - 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m - 1 * b][n].isItDestructable()) {
															block[m - 1 * b][n].setCollision(false);
															block[m - 1 * b][n].setDestructable(false);
															grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m - 1 * b][n].setBombExploded(true);
															grid[m - 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((m + 1 * b) < xGrid) {
													if (!grid[m + 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m + 1 * b][n].isItDestructable()) {
															block[m + 1 * b][n].setCollision(false);
															block[m + 1 * b][n].setDestructable(false);
															grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m + 1 * b][n].setBombExploded(true);
															grid[m + 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}else {
											//std::cout << "nelyginis" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((m - 1 * b) >= 0) {
													if (!grid[m - 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m - 1 * b][n].isItDestructable()) {
															block[m - 1 * b][n].setCollision(false);
															block[m - 1 * b][n].setDestructable(false);
															grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m - 1 * b][n].setBombExploded(true);
															grid[m - 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((n - 1 * b) >= 0) {
													if (!grid[m][n - 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n - 1 * b].isItDestructable()) {
															block[m][n - 1 * b].setCollision(false);
															block[m][n - 1 * b].setDestructable(false);
															grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n - 1 * b].setBombExploded(true);
															grid[m][n - 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((m + 1 * b) < xGrid) {
													if (!grid[m + 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m + 1 * b][n].isItDestructable()) {
															block[m + 1 * b][n].setCollision(false);
															block[m + 1 * b][n].setDestructable(false);
															grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m + 1 * b][n].setBombExploded(true);
															grid[m + 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}
									}
									else if ((i > 0 && i < xGrid - 1) && (j > 0 && j < yGrid - 1) && m == i && n == j) {
										//std::cout << "Visi kiti" << std::endl;
										if ((m % 2) == 0 && (n % 2) == 0) {
											//std::cout << "Visi kiti - Aukstyn, Zemyn, Kaire, Desine" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if (m - 1 * b >= 0) {
													if (!grid[m - 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m - 1 * b][n].isItDestructable()) {
															block[m - 1 * b][n].setCollision(false);
															block[m - 1 * b][n].setDestructable(false);
															grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m - 1 * b][n].setBombExploded(true);
															grid[m - 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((m + 1 * b) < xGrid) {
													if (!grid[m + 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m + 1 * b][n].isItDestructable()) {
															block[m + 1 * b][n].setCollision(false);
															block[m + 1 * b][n].setDestructable(false);
															grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m + 1 * b][n].setBombExploded(true);
															grid[m + 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((n - 1 * b) >= 0) {
													if (!grid[m][n - 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n - 1 * b].isItDestructable()) {
															block[m][n - 1 * b].setCollision(false);
															block[m][n - 1 * b].setDestructable(false);
															grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n - 1 * b].setBombExploded(true);
															grid[m][n - 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if (n + 1 * b < yGrid) {
													if (!grid[m][n + 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n + 1 * b].isItDestructable()) {
															block[m][n + 1 * b].setCollision(false);
															block[m][n + 1 * b].setDestructable(false);
															grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n + 1 * b].setBombExploded(true);
															grid[m][n + 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}else if ((m % 2) != 0 && (n % 2) == 0) {
											//std::cout << "Visi kiti - Kaire, Desine" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((m - 1 * b) >= 0) {
													if (!grid[m - 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m - 1 * b][n].isItDestructable()) {
															block[m - 1 * b][n].setCollision(false);
															block[m - 1 * b][n].setDestructable(false);
															grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m - 1 * b][n].setBombExploded(true);
															grid[m - 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m - 1 * b][n].isBombPlaced() || grid[m - 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m - 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m - 1 * b][n].setBombExploded(true);
														grid[m - 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((m + 1 * b) < xGrid) {
													if (!grid[m + 1 * b][n].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m + 1 * b][n].isItDestructable()) {
															block[m + 1 * b][n].setCollision(false);
															block[m + 1 * b][n].setDestructable(false);
															grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
															grid[m + 1 * b][n].setBombExploded(true);
															grid[m + 1 * b][n].setExplosionTime();
															break;
														}
														if (grid[m + 1 * b][n].isBombPlaced() || grid[m + 1 * b][n].hasBombExploded()) {
															break;
														}
														grid[m + 1 * b][n].setTexture("img/bomb_paths/boom.png");
														grid[m + 1 * b][n].setBombExploded(true);
														grid[m + 1 * b][n].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}

										}else if ((m % 2) == 0 && (n % 2) != 0) {
											//std::cout << "Visi kiti - Aukstyn, Zemyn2" << std::endl;
											for (int b = 1; b <= radius; b++) {
												if ((n + 1 * b) < yGrid) {
													if (!grid[m][n + 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n + 1 * b].isItDestructable()) {
															block[m][n + 1 * b].setCollision(false);
															block[m][n + 1 * b].setDestructable(false);
															grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n + 1 * b].setBombExploded(true);
															grid[m][n + 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n + 1 * b].isBombPlaced() || grid[m][n + 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n + 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n + 1 * b].setBombExploded(true);
														grid[m][n + 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
											for (int b = 1; b <= radius; b++) {
												if ((n - 1 * b) >= 0) {
													if (!grid[m][n - 1 * b].hasBombExploded()) {
														//DESTROY BLOCK
														if (block[m][n - 1 * b].isItDestructable()) {
															block[m][n - 1 * b].setCollision(false);
															block[m][n - 1 * b].setDestructable(false);
															grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
															grid[m][n - 1 * b].setBombExploded(true);
															grid[m][n - 1 * b].setExplosionTime();
															break;
														}
														if (grid[m][n - 1 * b].isBombPlaced() || grid[m][n - 1 * b].hasBombExploded()) {
															break;
														}
														grid[m][n - 1 * b].setTexture("img/bomb_paths/boom.png");
														grid[m][n - 1 * b].setBombExploded(true);
														grid[m][n - 1 * b].setExplosionTime();
														//std::cout << "TEST" << std::endl;
													}
												}
											}
										}
									}
								}
							}
							//explosionChainEvent(grid[i][j], xGrid, yGrid);
						}
						//std::cout << i << " " << j << " Time: " << grid[i][j].getBombCountDownTime() << std::endl;
					}
					if (!grid[i][j].getBombCountDownTime() && grid[i][j].hasBombExploded()) {
						grid[i][j].updateExplosionTimer(0.1234f);
						grid[i][j].removeExplosion();
						if (!grid[i][j].getExplosionCountDownTime()) {
							//Set back to default - bomb can be placed, explosion has not occured;
							//grid[i][j].setColor(sf::Color::Green);
							grid[i][j].setTexture("img/empty.png");
						}
						//std::cout << i << " " << j << " Time: " << grid[i][j].getExplosionCountDownTime() << std::endl;
					}
					//bombPlaced - 1 | = updateBombTimer, removeBomb (check if bomb should be removed)
					//bombTimer <= 0 | = bombExploded - 1, set
					//If bomb is placed then update bomb timer and check if it has to be removed and explosion to be enabled
					/*if (grid[i][j].isBombPlaced()) {
						grid[i][j].removeBomb();
						grid[i][j].updateBombTimer(0.1234f);
						grid[i][j].setExplosionTime();
					}
					if (grid[i][j].hasBombExploded()) {
						grid[i][j].removeExplosion();
						grid[i][j].updateExplosionTimer(0.1234f);
					}
					if (grid[i][j].removeExplosionFromTheArea()) {
						//grid[i][j].setRemoveExplosionFromTheArea(false);
						grid[i][j].removeExplosion();
						//grid[i][j].setBombUp(false);
					}
					if (grid[i][j].isBombPlaced() && !grid[i][j].hasBombExploded()) {
						//std::cout << "Draw bomb here" << std::endl;
						//grid[i][j].setTexture("img/bomb_paths/bomb.png");
						grid[i][j].setColor(sf::Color::Red);
					}else if (grid[i][j].hasBombExploded() && !grid[i][j].isBombPlaced()) {
						//std::cout << "Draw explosion here" << std::endl;
						//grid[i][j].setTexture("img/bomb_paths/boom.png");
						grid[i][j].setColor(sf::Color::Yellow);
					}*/
				}
			}

			//Teleport P1 / Set new position
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				p1.setPos({ (float)sf::Mouse::getPosition().x, (float)sf::Mouse::getPosition().y });
			}


			//Check if any of the players are in an explosion (an or a - grammar nazis where are you?)
			for (int i = 0; i < xGrid; i++) {
				for (int j = 0; j < yGrid; j++) {
					//Check if bomb has exploded
					if (!grid[i][j].checkIfTable() && grid[i][j].hasBombExploded()) {
						//Get bomb radius that was set by the player
						int radius = grid[i][j].getBombRadius();
						//If player is standing in their own explosion
						if (!p1.isImmune() && p1.isInExplosion(grid[i][j])) {
							p1.setHitTime(t);
						}
						if (!p2.isImmune() && p2.isInExplosion(grid[i][j])) {
							p2.setHitTime(t);
						}
						//block[i][j].setCollision(false);
						//0;0  - 12;0 
						//0;10 - 12;10
					}
				}
			}



			//!grid[i][j].checkIfTable() && grid[i][j].removeExplosionFromTheArea())


			//Set background to white
			window.clear(sf::Color::White);
			//Set background to map.png
			window.draw(map);

			//Draw grid to window
			//*Not adviced
			for (int i = 0; i < xGrid; i++) {
				for (int j = 0; j < yGrid; j++) {
					//std::cout << "Drawing" << std::endl;
					grid[i][j].drawTo(window);
				}
			}

			//Draw blocks to window
			for (int i = 0; i < xGrid; i++) {
				for (int j = 0; j < yGrid; j++) {
					if (block[i][j].isItActive()) {
						block[i][j].drawTo(window);
					}
				}
			}

			//Update players immunity timer
			p1.updateImmunityTimer(0.01456f);
			p2.updateImmunityTimer(0.01456f);
			//Reset players immunity
			p1.resetImmunity();
			p2.resetImmunity();

			//Update players bomb count timer
			p1.updateBombDropTimer(0.01456f);
			p2.updateBombDropTimer(0.01456f);
			//Reset players bomb update timer
			p1.resetBombCounterTimer();
			p2.resetBombCounterTimer();

			//Update player 1 position
			p1.Update(window, blockVec, &p2, gridVec);
			p1.drawTo(window);
			//Update player 2 position
			p2.Update(window, blockVec, &p1, gridVec);
			p2.drawTo(window);

			ssScore.str("");
			ssScore << "Score - P1: " << (int)p1.getScore() << ", P2: " << (int)p2.getScore() << ", Lives - P1: " << p1.getLives() << ", P2: " << p2.getLives();
			lblScore.setString(ssScore.str());

			//Draw the scoreboard
			window.draw(lblScore);
			//coin2.drawTo(window);
			//coin3.drawTo(window);
			window.display();
		}else {
			if (!mapTexture.loadFromFile("img/game_over.png")) {
				std::cout << "failed to load map.pgn" << std::endl;
			}
			
			if (!gameOverSound) {
				gameOverSound = true;
				if (!music.openFromFile("music/GaveOver.wav")) {
					std::cout << "no music found";
				}
				music.setVolume(50);         // reduce the volume
				music.setLoop(false);
				music.play();
			}
			map.setTexture(mapTexture);
			window.draw(map);
			window.draw(lblScore); //Draw score
			window.display();
		}
	}
	return 0;
}





