﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBM
{
    public class Worker{
        public string Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public string Bonus { get; set; }
        public int position { get; set; } //0 - worker, 1 - a fucking manager!, 2 - boss.

        public double CalculateBonus(){
            return (Salary * 1);
        }

        //constructor
        /*public Worker(string name, int NextId)
        {
            NextId += 10;
            Id = "stu" + NextId;
            Name = name;
        }*/
    }

    public class Manager : Worker{
        public double CalculateBonus(){
            return (Salary * 5.5);
        }
    }

    public class Boss : Worker
    {
        public double CalculateBonus()
        {
            return (Salary * 2);
        }
    }
}
