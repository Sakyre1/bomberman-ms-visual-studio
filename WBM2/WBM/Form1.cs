﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WBM{
    public partial class Form1 : Form{
        public Form1(){
            InitializeComponent();
        }
        Worker[] wr = new Worker[50];
        Manager[] mn = new Manager[50];
        Boss[] bs = new Boss[50];
        int n=0, m=0, b=0;
        int nextId = 100;

        private void comboBox1_DropDownClosed(object sender, EventArgs e)
        {
            if (comboBox1.Text != null || comboBox1.Text != "")
            {
                for (int i = 0; i < n; i++){
                    if (wr[i].Name == comboBox1.Text && comboBox1.Text != "" && wr[i].Name != ""){
                        radioButton1.Checked = true;
                        textBox1.Text = wr[i].Id;
                        textBox2.Text = wr[i].Name;
                        textBox3.Text = wr[i].Salary.ToString();
                        textBox4.Text = wr[i].CalculateBonus().ToString();
                    }
                }
                for (int i = 0; i < b; i++){
                    if (bs[i].Name == comboBox1.Text && comboBox1.Text != "" && bs[i].Name != ""){
                        radioButton2.Checked = true;
                        textBox1.Text = bs[i].Id;
                        textBox2.Text = bs[i].Name;
                        textBox3.Text = bs[i].Salary.ToString();
                        textBox4.Text = bs[i].CalculateBonus().ToString();
                    }
                }
                for (int i = 0; i < m; i++){
                    if (mn[i].Name == comboBox1.Text && comboBox1.Text != "" && mn[i].Name != ""){
                        radioButton3.Checked = true;
                        textBox1.Text = mn[i].Id;
                        textBox2.Text = mn[i].Name;
                        textBox3.Text = mn[i].Salary.ToString();
                        textBox4.Text = mn[i].CalculateBonus().ToString();
                    }
                }
                
            }
        }

        private void button1_Click(object sender, EventArgs e){
            if (textBox2.TextLength == 0){
                MessageBox.Show("name is demanded !");
                return;
            }


            if (radioButton1.Checked){
                wr[n] = new Worker();
                wr[n].Name = textBox2.Text;
                wr[n].Id = nextId.ToString();
                comboBox1.Items.Add(wr[n].Name);
                textBox1.Text = wr[n].Id;
                wr[n].Salary = int.Parse(textBox3.Text);
                wr[n].position = 0;
                n++;
                nextId += 10;
            }else if (radioButton2.Checked){
                bs[b] = new Boss();
                bs[b].Name = textBox2.Text;
                bs[b].Id = nextId.ToString();
                comboBox1.Items.Add(bs[b].Name);
                textBox1.Text = bs[b].Id;
                bs[b].Salary = int.Parse(textBox3.Text);
                bs[b].position = 0;
                b++;
                nextId += 10;
            }
            else{
                mn[m] = new Manager();
                mn[m].Name = textBox2.Text;
                mn[m].Id = nextId.ToString();
                comboBox1.Items.Add(mn[m].Name);
                textBox1.Text = mn[m].Id;
                mn[m].Salary = int.Parse(textBox3.Text);
                mn[m].position = 0;
                m++;
                nextId += 10;
            }
        }
    }
}
