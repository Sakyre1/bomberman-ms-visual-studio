#pragma once

#include <iostream>
#include <SFML\Graphics.hpp>

class Block {
	public:
		void setDefaultStyle() {
			//block.setSize({64, 64});
			//block.setPosition({4000, 4000});
			//pcSprite.setPosition({4000, 4000});
			//block.setFillColor(sf::Color::Transparent);
		}
		void setAsSpawn() {
			active = false;
			destroyable = true;
		}
		void setCollision(bool activity) {
			active = activity;
		}
		void setStyle(sf::Vector2f size, sf::Vector2f position, sf::Color color, bool activity, bool destructable, sf::String pcT) {
			if (!pcTexture.loadFromFile(pcT)) {
				std::cout << "Error loading resource '::Texture'" << std::endl;
			}
			pcSprite.setTexture(pcTexture);
			pcSprite.setPosition(position);
			block.setSize(size);
			block.setPosition(position);
			//block.setFillColor(color);
			active = activity;
			destroyable = destructable;
		}

		void drawTo(sf::RenderWindow &window) {
			window.draw(pcSprite);
		}

		sf::FloatRect getGlobalBounds() {
			return block.getGlobalBounds();
		}

		bool isItActive() {
			return active;
		}

		bool isItDestructable() {
			return destroyable;
		}

		void setDestructable(bool ans) {
			destroyable = ans;
		}
		void setPos(sf::Vector2f newPos) {
			block.setPosition(newPos);
		}
		void setNewTexture(sf::String newText) {
			pcTexture.loadFromFile(newText);
			pcSprite.setTexture(pcTexture);
		}
	private:
		bool active = true;
		bool destroyable;
		sf::Texture pcTexture;
		sf::Sprite pcSprite;
		sf::RectangleShape block;
};