#pragma once

#include <iostream>
#include <string>
#include <SFML\Graphics.hpp>

class Grid {
	public:
		void setStyle(sf::Vector2f size, sf::Vector2f position, sf::Color newColor) {
			gridBlock.setSize(size);
			gridBlock.setPosition(position);
			gridBlock.setFillColor(newColor);
			pcSprite.setPosition(position);
		}
		void setColor(sf::Color chosenColor) {
			gridBlock.setFillColor(chosenColor);
		}
		sf::FloatRect getGlobalBounds() {
			return gridBlock.getGlobalBounds();
		}
		void setItAsTable(bool answer) {
			isItATable = answer;
		}
		int checkIfTable() {
			return isItATable;
		}
		void setBombUp(bool answer) {
			//this->bombPlaced = true;
			bombPlaced = answer;
		}
		bool isBombPlaced() {
			return bombPlaced;
		}
		/*void playerIsOnBlock(bool answer) {
			isPlayerOnBlock = answer;
		}*/
		void drawTo(sf::RenderWindow &window) {
			//window.draw(gridBlock);
			window.draw(gridBlock);
			window.draw(pcSprite);
		}
		void setName(std::string newName) {
			gridBlockXY = newName;
		}
		void setXY(int setX, int setY) {
			x = setX, y = setY;
		}
		void setBombRadius(int playerBombRadius) {
			bombRadius = playerBombRadius;
		}
		//Set up bomb timer
		void setBombTime() {
			if (bombCurrentCountDownTime <= 0 && bombPlaced) {
				bombCurrentCountDownTime = defaultCountDownTime;
				//std::cout << "Setting ON immunity " << p_name << " " << immuneToDamage << " " << immunityTimer << std::endl;
			}
		}
		//Set the time for explosion to last for
		void setExplosionTime() {
			if (explosionCurrentCountDownTime <= 0 && bombExploded) {
				explosionCurrentCountDownTime = defaultExplosionTime;
				//std::cout << "Setting ON immunity " << p_name << " " << immuneToDamage << " " << immunityTimer << std::endl;
			}
		}
		//Update explosion timer
		void updateExplosionTimer(float currentTime) {
			if (explosionCurrentCountDownTime > 0) {
				explosionCurrentCountDownTime -= currentTime;
				if (explosionCurrentCountDownTime <= 0) {
					explosionCurrentCountDownTime = 0;
				}
				//std::cout << explosionCurrentCountDownTime << " bombPlaced:" << bombPlaced << " bombExploded:" << bombExploded << std::endl;
			}
		}
		//Update bomb timer
		void updateBombTimer(float currentTime) {
			if (bombCurrentCountDownTime > 0) {
				bombCurrentCountDownTime -= currentTime;
				if (bombCurrentCountDownTime <= 0) {
					bombCurrentCountDownTime = 0;
				}
				//std::cout << bombCurrentCountDownTime << std::endl;
			}
		}
		//Check if bomb has exploded
		bool hasBombExploded() {
			//std::cout << bombExploded << std::endl;
			return bombExploded;
		}
		//If bomb exploded set it to true. (Did I even use it?)
		void setBombExploded(bool ans) {
			bombExploded = ans;
		}
		//Remove bomb from the area after the count down timer reached 0
		void removeBomb() {
			if (bombCurrentCountDownTime <= 0 && bombPlaced) {
				//Bomb has exploded to remove bomb - set it to false
				bombPlaced = false;
				//Change from BOMB to EXPLOSION
				bombExploded = true;
				bombCurrentCountDownTime = 0;
				bombCollidable = false; // Removed collision from the bomb
				//gridBlock.setFillColor(sf::Color::Black);
				//std::cout << "Bomb has exploded " << bombPlaced << std::endl;
			}
		}
		//Removed explosion from the area after the explosion count down timer reached 0
		void removeExplosion() {
			if (explosionCurrentCountDownTime <= 0 && bombExploded) {
				bombExploded = false;
				explosionCurrentCountDownTime = 0;
				//setTexture("img/empty.png");
				//std::cout << "Explosion has faded " << bombExploded << std::endl;
			}
		}
		//Enable bomb collision (so no player can pass)
		void enableBombCollision(bool ans) {
			bombCollidable = ans;
		}
		//Update bomb collision count down to enable collision
		void updateCollisionTimer(float currentTime) {
			if ((bombCurrentCountDownTime < (defaultCountDownTime - 3.5f)) && !bombCollidable && bombPlaced) {
				bombCollidable = true;
				//std::cout << bombCurrentCountDownTime << " " << (defaultCountDownTime - 2) << std::endl;
				//std::cout << bombCurrentCountDownTime << std::endl;
			}
		}
		//Return if bomb is collidable
		bool isBombCollidable() {
			return bombCollidable;
		}
		//Check explosion count down time
		float getExplosionCountDownTime() {
			return explosionCurrentCountDownTime;
		}
		//Check bomb count down time
		float getBombCountDownTime() {
			return bombCurrentCountDownTime;
		}
		//Do not use
		bool removeExplosionFromTheArea() {
			return removeAreaExplosion;
		}
		//Do not use
		void setRemoveExplosionFromTheArea(bool ans) {
			removeAreaExplosion = ans;
		}
		void setTexture(sf::String pcT) {
			if (!pcTexture.loadFromFile(pcT)) {
				//std::cout << "Error loading resource '::Texture'" << std::endl;
			}
			pcSprite.setTexture(pcTexture);
			//pcSprite.setPosition(position);
		}
		int getBombRadius() {
			return bombRadius;
		}
		sf::Vector2f getXY() {
			return {(float)x, (float)y};
		}
		std::string getName() {
			return gridBlockXY;
		}
	private:
		std::string gridBlockXY;
		int x, y;
		bool bombExploded = false;
		bool bombCollidable = false; //If its true then stop letting player pass through (those that got stuck...too bad)
		bool bombPlaced = false; //Bomb is placed by a player. This against 2 bombs on the same grid position
		float bombCurrentCountDownTime = 0.0f; //If bomb was placed set 'bombCurrentCountDownTime' to be equal to 'defaultCountDownTime'
		float explosionCurrentCountDownTime = 0.0f;//If count down reached 0 set 'explosionCurrentCountDownTime' to be equal to ' defaultExplosionTime'
		float defaultCountDownTime = 25.0f; //Default numbers in the future they should be set by one of the players
		float defaultExplosionTime = 10.0f; //Default numbers in the future they should be set by one of the players
		int bombRadius; //This is set by the Player how far can the explosion can reach. Note: ATM explosion goes through all blocks which is kinda bad.
		//bool isPlayerOnBlock = false;
		sf::Texture pcTexture;
		sf::Sprite pcSprite;
		sf::RectangleShape gridBlock;
		bool isItATable = false;
		bool removeAreaExplosion = false; //After time runs out check it to true to know that it must be removed
};